import React from 'react';

import {Hero} from './component/ui/hero';
import {Usecases} from './component/ui/usecases';

export default function Home() {
  return (
   <React.Fragment>
    <Hero />
    <Usecases />
   </React.Fragment>
  );
}
