from flask import Flask, request
from flask_cors import CORS
import openai

app = Flask(__name__)

@app.route("/api", methods=["GET"])
def hello_world():
    return 'Welcome to GramAssit API!'


# TODO: Change to Open source API key


# Set up your OpenAI API credentials
# openai.api_key=""

prompts = {
    "summary": "You are a resume professional. Generate a summary of the candidate's qualifications.",
    "experience": "You are a resume professional. Provide details of the candidate's work experience.",
    "education": "You are a resume professional. Provide details of the candidate's education.",
}


@app.route("/api/resume/block/<string:component>", methods=["GET"])
def get_resume_block(component):
    response = openai.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": prompts[component]
            },
            {
                "role": "user",
                "content": "Candidate's name: John Doe\nExperience: 5 years of software development\nSkills: Python, Java, JavaScript\nEducation: Bachelor's degree in Computer Science\n",
            }
        ],
        model="gpt-3.5-turbo",
    )
    summary = response.choices[0].message.content
    return summary
    
@app.route("/api/resume/check", methods=["POST"])
def check_resume_blocks():
    resume_blocks = request.json["blocks"]
    suggestions = []
    for block in resume_blocks:
        if(block["message"]):
            response = openai.chat.completions.create(
                model="gpt-3.5-turbo",
                messages=[
                    {"role": "system", "content": "You are a resume professional. Check the tone, grammar, and punctuation of the resume block."},
                    {"role": "user", "content": block["message"]},
                ],
            )
            suggestion = response.choices[0].message.content
            suggestions.append(suggestion)
    return suggestions



def build_user_profile():
    return {
        "name": "John Doe",
        "experience": "5 years of software development",
        "skills": "Python, Java, JavaScript",
        "education": "Bachelor's degree in Computer Science",
    }







