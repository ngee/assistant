"use client"
import { Resume, BlankNote, Feedback } from "../../../component/usecases";
import Suggestions from "../../../component/ui/suggestions";
import { useState } from "react";
export interface EditorPageProps {
  params: {
    id: string;
  };
}

const Editor = ({ params }: EditorPageProps) => {
  const { id } = params || "resume";

  const [suggestions, setSuggestions] = useState([]);

  const component = {
    resume: <Resume setSuggestions={setSuggestions}/>,
    "blank-note": <BlankNote />,
    feedback: <Feedback />,
  }[id];

  return (
    <div className="grid gap-4 md:grid-cols-6 px-4">
      <div className="col-span-4">{component}</div>
      <div className="col-span-1">
      <Suggestions options={suggestions} />
      </div>
    </div>
  );
};

export default Editor;
