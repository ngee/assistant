import "./index.css";
import * as Fetchers from "./action";

export default class AskAi {
  data: any;
  api: any;
  readOnly: boolean;
  titlePlaceholder: string;
  messagePlaceholder: string;
  restAPI: any;
  wrapper: any;
  container: any;

  constructor({
    data,
    config,
    api,
    readOnly,
  }: {
    data: any;
    config: any;
    api: any;
    readOnly: boolean;
  }) {
    this.api = api;
    this.readOnly = readOnly;
    this.restAPI = Fetchers.getResumeBlock;
    this.container = null;

    this.titlePlaceholder =
      config.titlePlaceholder || AskAi.DEFAULT_TITLE_PLACEHOLDER;
    this.messagePlaceholder =
      config.messagePlaceholder || AskAi.DEFAULT_MESSAGE_PLACEHOLDER;

    this.data = {
      title: data.title || "",
      message: data.message || "",
    };
  }

  static get enableLineBreaks() {
    return true;
  }

  static get DEFAULT_TITLE_PLACEHOLDER() {
    return "Enter a title";
  }

  static get DEFAULT_MESSAGE_PLACEHOLDER() {
    return `Add description...`;
  }

  static get toolbox() {
    return {
      title: "Ask AI",
    };
  }

  get CSS() {
    return {
      baseClass: this.api.styles.block,
      wrapper: "cdx-ask-ai-block",
      title: "cdx-ask-ai-block__title",
      input: this.api.styles.input,
      message: "cdx-ask-ai-block__message",
    };
  }


  render() {
    const containerStyles = [this.CSS.baseClass, this.CSS.wrapper]
    const titleStyles = [this.CSS.input, this.CSS.title]
    const messageStyles = [this.CSS.input, this.CSS.message]

    this.container = this._make("div", containerStyles);

    const title = this._make("div", titleStyles, {
      contentEditable: !this.readOnly,
      innerHTML: this.data.title,
    });

    const message = this._make("div", messageStyles, {
      contentEditable: !this.readOnly,
      innerHTML: this.data.message,
    });

    title.dataset.placeholder = this.titlePlaceholder;
    message.dataset.placeholder = this.messagePlaceholder;

    this.restAPI(this.data.title.toLowerCase())
      .then((response: any) => {
        message.innerHTML = response;
      })
      .catch((error: any) => {
        console.error(error);
      });

    this.container.appendChild(title);
    this.container.appendChild(message);
    return this.container;
  }

  async save(blockElement: any) {
    const title = blockElement.querySelector(`.${this.CSS.title}`);
    const message = blockElement.querySelector(`.${this.CSS.message}`);

    return Object.assign(this.data, {
      title: title.innerHTML,
      message: message.innerHTML,
    });
  }

  _make(
    tagName: any,
    classNames: any = null,
    attributes: { [key: string]: string | boolean } = {}
  ) {
    const el = document.createElement(tagName);

    if (Array.isArray(classNames)) {
      el.classList.add(...classNames);
    } else if (classNames) {
      el.classList.add(classNames as string);
    }
    for (const attrName in attributes) {
      el[attrName] = attributes[attrName];
    }
    return el;
  }

  static get sanitize() {
    return {
      title: {},
      message: {},
    };
  }
}
