import axios from 'axios';

export const getAPI = async () => {
  try {
    const response = await axios.get(`/api/python`);
    return response.data;
  } catch (error) {
    throw new Error('Failed to create client');
  }
}

export const getResumeBlock = async (component: string) => {
  try {
    const response = await axios.get(`/api/resume/block/${component}`);
    return response.data;
  } catch (error) {
    throw new Error('Failed to create client');
  }
}

export const checkResumeBlock = async (blocks: []) => {
  try {
    const response = await axios.post(`/api/resume/check`, { blocks });
    return response.data;
  } catch (error) {
    throw new Error('Failed to create client');
  }
}