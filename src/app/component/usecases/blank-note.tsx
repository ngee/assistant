"use client";
import React, { useRef, useEffect } from "react";
import EditorJS from "@editorjs/editorjs";
import Header from "@editorjs/header";

const BlankNote: React.FC = () => {
  const ejInstance = useRef();

  const DEFAULT_INITIAL_DATA = {
    time: new Date().getTime(),
    blocks: [
      {
        type: "header",
        data: {
          text: "Blank Note",
          level: 1,
        },
      }
    ],
  };

  const initEditor = () => {
    const editor = new EditorJS({
      holder: "editorjs",
      onReady: () => {
        //@ts-ignore
        ejInstance.current = editor;
      },
      // readOnly: true,
      data: DEFAULT_INITIAL_DATA,
      onChange: async () => {
        let content = await editor.saver.save();
      },
      tools: {
        header: Header,
      },
    });
  };

  useEffect(() => {
    if (ejInstance.current === null) {
      initEditor();
    }

    return () => {
      // @ts-ignore
      ejInstance?.current?.destroy();
      // @ts-ignore
      ejInstance.current = null;
    };
  }, []);
  return (
    <div>
      <div id="editorjs"></div>
    </div>
  );
};

export default BlankNote;
