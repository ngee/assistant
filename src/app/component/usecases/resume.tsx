"use client";
import React, { useRef, useEffect } from "react";
import EditorJS from "@editorjs/editorjs";
import Header from "@editorjs/header";
import AskAi from "../editor/ask-ai";
import * as Fetchers from '../editor/action'

const createdAt = new Date().getTime();

const Resume = ({ setSuggestions }: any) => {
  const ejInstance = useRef();

  const [lastUpdated, setLastUpdated] = React.useState<number | undefined>(
    undefined
  );

  const [data, setData] = React.useState<any>(null);


  const requestSuggestions = async () => {
    try {
      const response = await Fetchers.checkResumeBlock(data);
      console.log(response, 'response')
      setSuggestions(response);
    } catch (error) {
      console.error(error);
    }
  };


  useEffect(() => {
    requestSuggestions()
  }, [data]);


  const DEFAULT_INITIAL_DATA = {
    time: new Date().getTime(),
    blocks: [
      {
        type: "askAI",
        data: {
          title: "Summary",
          message: "",
        },
      },
      {
        type: "askAI",
        data: {
          title: "Experience",
          message: "",
        },
      },
      {
        type: "askAI",
        data: {
          title: "Education",
          message: "",
        },
      },
    ],
  };

  const initEditor = () => {
    const editor = new EditorJS({
      holder: "editorjs",
      placeholder: "Start creating your resume sections",
      onReady: () => {
        //@ts-ignore
        ejInstance.current = editor;
      },
      // readOnly: true,
      data: DEFAULT_INITIAL_DATA,
      onChange: async () => {
        let content = await editor.saver.save();
        const formattedData = content.blocks.map((block: any) => {
          return {
            title: block.data.title,
            message: block.data.message,
          }
        })
        setData(formattedData);
        setLastUpdated(content?.time || undefined);
      },
      tools: {
        header: Header,
        askAI: AskAi,
      },
    });
  };

  useEffect(() => {
    if (ejInstance.current === null) {
      initEditor();
    }

    return () => {
      // @ts-ignore
      ejInstance?.current?.destroy();
      // @ts-ignore
      ejInstance.current = null;
    };
  }, []);


  const intervals = [
    { label: 'year', seconds: 31536000 },
    { label: 'month', seconds: 2592000 },
    { label: 'day', seconds: 86400 },
    { label: 'hour', seconds: 3600 },
    { label: 'minute', seconds: 60 },
    { label: 'second', seconds: 1 }
  ];
  
  function timeAgo(date: number) {
    const seconds = Math.floor((date - createdAt ) / 1000);
    const interval = intervals.find(i => i.seconds < seconds) as { label: string, seconds: number };
    const count = Math.floor(seconds / interval?.seconds);
    return interval ? `${count} ${interval?.label}${count !== 1 ? 's' : ''} ago` : `Just now`;
  }

  return (
    <div>
      <div
        style={{ maxWidth: "640px", margin: "auto", marginBottom: "1rem" }}
        className="text-xs text-gray-500"
      >
        {lastUpdated && (
          <span>
            last updated:{" "}
            {timeAgo(lastUpdated)}
          </span>
        )}
      </div>
      <div id="editorjs"></div>
    </div>
  );
};

export default Resume;
