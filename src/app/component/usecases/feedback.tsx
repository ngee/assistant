"use client";
import React, { useRef, useEffect } from "react";
import EditorJS from "@editorjs/editorjs";
import Header from "@editorjs/header";
import AskAi from "../editor/ask-ai";

const createdAt = new Date().getTime();

const Feedback: React.FC = () => {
  const [lastUpdated, setLastUpdated] = React.useState<number | undefined>(
    undefined
  );
  const [data, setData] = React.useState<any>(null);

  const ejInstance = useRef();

  const DEFAULT_INITIAL_DATA = {
    time: new Date().getTime(),
    blocks: [
      {
        type: "askAI",
        data: {
          title: "Summary",
          message: "",
        },
      },
    ],
  };

  const initEditor = () => {
    const editor = new EditorJS({
      holder: "editorjs",
      onReady: () => {
        //@ts-ignore
        ejInstance.current = editor;
      },
      // readOnly: true,
      data: DEFAULT_INITIAL_DATA,
      onChange: async () => {
        let content = await editor.saver.save();
        const formattedData = content.blocks.map((block: any) => {
          return {
            title: block.data.title,
            message: block.data.message,
          }
        })
        console.log(formattedData);
        setLastUpdated(content?.time || undefined);
      },
      tools: {
        header: Header,
        askAI: AskAi,
      },
    });
  };

  useEffect(() => {
    if (ejInstance.current === null) {
      initEditor();
    }

    return () => {
      // @ts-ignore
      ejInstance?.current?.destroy();
      // @ts-ignore
      ejInstance.current = null;
    };
  }, []);
  return (
    <div>
      <div id="editorjs"></div>
    </div>
  );
};

export default Feedback;
