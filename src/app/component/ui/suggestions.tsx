"use client";
import React from "react";
import * as Collapsible from "@radix-ui/react-collapsible";
import { RowSpacingIcon, Cross2Icon } from "@radix-ui/react-icons";

const Suggestions = ({ options }: any) => {
  const [open, setOpen] = React.useState(false);

  return (
    <Collapsible.Root className="w-[300px]" open={open} onOpenChange={setOpen}>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <span className="text-red text-[15px] leading-[25px]">
          {`${options.length} suggestions`}
        </span>
        <Collapsible.Trigger asChild>
          <button className="rounded-full h-[25px] w-[25px] inline-flex items-center justify-center  outline-none data-[state=closed]:bg-white data-[state=open]:bg-violet3 hover:bg-violet3">
            {open ? <Cross2Icon /> : <RowSpacingIcon />}
          </button>
        </Collapsible.Trigger>
      </div>

      <Collapsible.Content>
        {options &&
          options.map((o: any, index: number) => {
            return (
              <div
                className="bg-white rounded my-[10px] p-[10px]  shadow-blackA4"
                key={index}
              >
                <span className="text-red text-[15px] leading-[25px]">{o}</span>
              </div>
            );
          })}
      </Collapsible.Content>
    </Collapsible.Root>
  );
};

export default Suggestions;
