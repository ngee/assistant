const Hero = () => {
  return (
    <section className="bg-gradient-to-r from-[#DEF1EF] to-[#F1E3F6]">
      <div className="py-8 px-4 mx-auto max-w-screen-xl sm:py-16 lg:px-6">
        <h1 className="mb-4 text-2xl tracking-tight leading-none text-black md:text-2xl lg:text-5xl font-bold">
          Discover the power of GramAssit
        </h1>
        <p className="mb-8 text-lg font-normal lg:text-xl">
          Transforming your writing with precision and ease using AI.
        </p>
        <div className="flex flex-col space-y-4 sm:flex-row sm:space-y-0">
        <button className="bg-[#1C65C1] text-white font-bold py-2.5 px-14 rounded-[100px] text-sm">
          Get Started
        </button>
        </div>
      </div>
    </section>
  );
};

export { Hero };
