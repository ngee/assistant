"use client";

import React from "react";
import { usePathname } from "next/navigation";

function Header() {
  const pathname = usePathname();

  const slug = pathname.split("/").pop();

  return (
    <header className="sticky top-0 z-50 flex items-center justify-between w-full h-16 px-4 shrink-0 bg-gradient-to-b from-background/10 via-background/50 to-background/80 backdrop-blur-xl">
      <div className="flex items-center">
        <a className="font-bold" href="/">
          GramAssit / {slug}
        </a>
      </div>
      <div className="flex items-center justify-end gap-2">
      </div>
    </header>
  );
}

export { Header };
