import Link from "next/link";

type CardProps = {
  title: string;
  description: string;
  link: string;
};

export const Card = ({ title, description, link }: CardProps) => {
  return (
    <Link
      href={link}
      className="block max-w-sm p-6 bg-[white] rounded-lg shadow hover:bg-gray-100"
    >
      <h5 className="mb-2 text-2xl font-bold tracking-tight text-black">
        {title}
      </h5>
      <p className="font-normal text-black">
        {description}
      </p>
    </Link>
  );
};
